using HWO.Messages;
using HWO.SerializableObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace HWO
{
    public class bot
    {
        private StreamWriter _writer;
        private TrackData _trackData;
        private CarData _carData;
        private string _myCar;
        private Turbo _turbo = new Turbo(0, 0, 0);

		private bool _readyToSwitchLane = true;
		private int _noOfLanes;

        private SpeedCalculator _speedCalculator = new SpeedCalculator();
        private int _speedsCaptured;

        public bot(StreamReader reader, StreamWriter writer, IStartRace join)
        {
            _turbo.Use();
            _writer = writer;
            string line;
            

            send(join as SendMsg);

            while ((line = reader.ReadLine()) != null)
            {
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                try
                {
                    switch (msg.msgType)
                    {
                        case "carPositions":
                            //get target speed for car on next piece
                            //send throttle needed to get to target speed by then
                            //use car positions to update target speeds on track data.
                            var carPositions = JsonConvert.DeserializeObject<CarPosition[]>(msg.data.ToString());
                            var prevSpeed = _carData.Speed(_myCar);
                            _carData.SetCarPositions(carPositions, _trackData);

                            var speed = _carData.Speed(_myCar);
                            if(speed > 0)
                                _speedsCaptured++;
                            if (_speedsCaptured == 0)
                            {
                                send(new Throttle(1.0));
                                break;
                            }
                            if (_speedsCaptured == 1)
                            {
                                _speedCalculator.MaxAcceleration = speed;
                                _speedCalculator.Speed1 = speed;
                                send(new Throttle(1.0));
                            }
                            if (_speedsCaptured == 2)
                            {
                                _speedCalculator.Speed2 = speed;
                                _speedCalculator.CalculateEnginePower();
                                send(new Throttle(0.1));
                                break;
                            }
                            if (!_speedCalculator.EnginePowerCalculated)
                            {
                                TryCalculateEnginePower(prevSpeed, speed);
                                send(new Throttle(0.1));
                                break;
                            }
                            var piecePos = _carData.Position(_myCar).piecePosition;
                            var lane = piecePos.lane.endLaneIndex;
                            var nextPieceIndex = piecePos.pieceIndex + 1;
 							if (_trackData.getSwitchDirection(nextPieceIndex)==null)
						        _readyToSwitchLane = true; //why is it ready to switch if it has no switch direction?
                            
							if (_turbo.IsAvailable && speed > 0)
                            {
                                if (_trackData.DistanceToNextTurn(piecePos.pieceIndex, piecePos.inPieceDistance, piecePos.lane.endLaneIndex) > speed * _turbo.turboDurationTicks)
                                {
                                    _turbo.Use();
                                    Console.WriteLine("Turbo used");
                                    send(new TurboMsg());
                                    break;
                                }
                            }

							if (!handleLaneSwitching())	//If switchLane message not sent
							{
                                var requiredSpeedForNextPiece = _trackData.TargetSpeed(nextPieceIndex, lane);
                                var requiredSpeedForThisPiece = _trackData.TargetSpeed(piecePos.pieceIndex, lane);

                                var requiredSpeed = _speedCalculator.CalculateRequiredSpeed(requiredSpeedForNextPiece, requiredSpeedForThisPiece, _trackData.DistanceBetween(piecePos.pieceIndex, piecePos.inPieceDistance, nextPieceIndex, lane));
                                var throttle = _speedCalculator.DetermineThrottle(speed, requiredSpeed);
                                send(new Throttle(throttle));                        
							}
                            break;
                        case "join":
                            Console.WriteLine("Joined");
                            send(new Ping());
                            break;
                        case "gameInit":
                            var race = JsonConvert.DeserializeObject<Race>((msg.data as JObject)["race"].ToString());

                            if (_trackData == null)
                            {
                                _trackData = new TrackData(race.track);
                                _carData = new CarData(race.cars);
								_noOfLanes = race.track.lanes.Length;
                            }

                            Console.WriteLine("Race init");
                            send(new Ping());
                            break;
                        case "yourCar":
                            _myCar = ((JObject)msg.data)["color"].ToString();
                            break;
                        case "gameEnd":
                            Console.WriteLine("Race ended");
                            break;
                        case "gameStart":
                            Console.WriteLine("Race starts");
                            break;
                        case "crash":
                            var TEMP_DECEL_CONST = 1;
                            var color = ((JObject)msg.data)["color"].ToString();
                            var crashPos = _carData.Position(color).piecePosition;
                            var pieceNum = crashPos.pieceIndex;
                            var crashSpeed = _carData.Speed(color);
                            if(_trackData.TargetSpeed(pieceNum, crashPos.lane.endLaneIndex) > crashSpeed)
                                _trackData.SetTargetSpeed(pieceNum, _carData.Speed(color), _speedCalculator.MaxAcceleration, _speedCalculator, _carData.Lane(color));
                            Log(msg);
                            break;
                        case "turboAvailable":
                            _turbo = JsonConvert.DeserializeObject<Turbo>(msg.data.ToString());
                            Console.WriteLine("Turbo available");
                            break;
                        default:
                            Log(msg);
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(string.Format("Error: {0}\nMsgType: {1}\nData: {2}", e.Message, msg.msgType, msg.data));
                    Console.WriteLine(string.Format(msg.msgType));
                    send(new Ping());
                }
            }
        }

        private void TryCalculateEnginePower(double prevSpeed, double speed)
        {
            Console.WriteLine("Speed: " + speed);
            double enginePower;
            if (Math.Abs(prevSpeed - speed) < 0.0001)
            {
                enginePower = Math.Min(prevSpeed, speed) / 0.1;
                Console.WriteLine("Top speed reached. Engine power: " + enginePower);
                _speedCalculator.EnginePower = enginePower;
            }
            else if (_carData.IsCarCloseBehind(_myCar, _trackData))
            {
                enginePower = prevSpeed / 0.1;
                Console.WriteLine("Car is close behind. Engine power: " + enginePower);
                //_speedCalculator.EnginePower = enginePower;
            }
        }

        private void Log(MsgWrapper msg)
        {
            Console.WriteLine(msg.msgType);
            if (msg.data != null)
                Console.WriteLine(msg.data.ToString());
        }

        private void send(SendMsg msg)
        {
            _writer.WriteLine(msg.ToJson());
        }

		private bool handleLaneSwitching() //returns true if lane switch message sent
		{
			if (_trackData.getSwitchDirection(_carData.Position(_myCar).piecePosition.pieceIndex+1)==null)
		        _readyToSwitchLane = true;

			//Send switch lane message if the next piece is a switch
			if (_readyToSwitchLane && (_trackData.getSwitchDirection(_carData.Position(_myCar).piecePosition.pieceIndex+1)!=null))
			{
				_readyToSwitchLane = false;
				Console.WriteLine("Next piece is a switch");
				string switchDirection = _trackData.getSwitchDirection(_carData.Position(_myCar).piecePosition.pieceIndex+1);
				if (switchDirection == "Straight")
				{
					switchDirection	= null;
					Console.WriteLine("Best switch direction ignoring traffic: Straight");
				}
				else {
					Console.WriteLine("Best switch direction ignoring traffic: "+ switchDirection);
				}

				switchDirection = findBestSwitchDirectionConsideringTraffic(switchDirection);

				if (switchDirection!=null)
				{
					Console.WriteLine("switchDirection for this switch is "+switchDirection);
					send (new SwitchLane(switchDirection));
				}
				else {
					Console.WriteLine("switchDirection for this switch is straight");
					return false;
				}
				return true;
			}
			else
				return false;
		}

		private string findBestSwitchDirectionConsideringTraffic (string switchDirection)
		{
			int myLane = _carData.Lane (_myCar);
			Console.WriteLine ("My car is in lane " + myLane);

			//Count number of cars in other lanes
			int[] carsInLanes = new int[_noOfLanes];
			int bestLaneIgnoringTraffic = myLane;
			int emptiestLane;

			//Find the best lane ignoring traffic
			switch (switchDirection) {
			case "Right":
				if (bestLaneIgnoringTraffic < (_noOfLanes-1)) {
					bestLaneIgnoringTraffic++;
				}
				break;
			case "Left":
				if (bestLaneIgnoringTraffic > 0) {
					bestLaneIgnoringTraffic--;
				}
				break;
			default:
				break;
			}

			foreach (var car in _carData.getCars()) {
				if ((car.Key != _myCar) && (isCarBetweenMyCarAndTheSwitchAfterTheNextSwitch((int)_carData.Position(_myCar).piecePosition.pieceIndex, (int)car.Value.Position.piecePosition.pieceIndex))) {
					Console.WriteLine("Car: "+ car.Key + " is at position: " + (int)car.Value.Position.piecePosition.pieceIndex);
					carsInLanes [car.Value.Position.piecePosition.lane.endLaneIndex]++;
				}
			}

			//Get the emptiest lane
			emptiestLane = Array.IndexOf (carsInLanes, carsInLanes.Min ());
			//Console.WriteLine ("Emptiest lane is " + emptiestLane);

			//Print out counts of cars in lanes
			//Console.WriteLine("Numbers of cars in lanes before switching");
			//Console.WriteLine("-----------------------------------------");
			//for (int i = 0; i < carsInLanes.Length; i++) {
			//	Console.WriteLine("Lane " + i + " has " + carsInLanes[i] + " cars");
			//}
			//Console.WriteLine("-----------------------------------------");

			if (carsInLanes [bestLaneIgnoringTraffic] == carsInLanes [emptiestLane]) { //best lane without traffic is one of the emptiest
				Console.WriteLine ("Switch to best lane ignoring traffic: " + bestLaneIgnoringTraffic);
				return switchDirection;
			} else if (carsInLanes [emptiestLane] == carsInLanes [myLane]) {
				Console.WriteLine("Stay in the same lane: " + myLane);
				return null;
			}
			else
			{
				Console.WriteLine("Switch to emptiest lane");
				if (emptiestLane < myLane) {
					return "Left";
				} else if (emptiestLane > myLane) {
					return "Right";
				}
				else
				{
					return null;
				}
			}
		}

		private bool isCarBetweenMyCarAndTheSwitchAfterTheNextSwitch(int myCarPieceIndex, int otherCarPieceIndex)
		{
			int switchIndex = _trackData.getNextSwitchIndex(_trackData.getNextSwitchIndex(myCarPieceIndex));
			//Console.WriteLine("Switch after the next switch is on piece: " + switchIndex);

			if (myCarPieceIndex < switchIndex)
			{
				return ((otherCarPieceIndex > myCarPieceIndex) && (otherCarPieceIndex < switchIndex));
			}
			else
			{
				return ((otherCarPieceIndex < switchIndex) || (otherCarPieceIndex>myCarPieceIndex));
			}
		}
	}
}