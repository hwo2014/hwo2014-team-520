﻿using HWO.SerializableObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HWO
{
    public class TrackData
    {

        private List<ITrackPiece> _trackPieces = new List<ITrackPiece>();
        private int _lanes;
        public TrackData(Track track)
        {
            List<double> lanes = GetLaneDists(track.lanes);
            _lanes = lanes.Count;
            _trackPieces = track.pieces.Select(piece => (ITrackPiece)CreateTrackPiece(piece, lanes)).ToList();
            calculateLaneSwitches();
        }

        private List<double> GetLaneDists(dynamic[] lanes)
        {
            var dists = new List<double>();
            foreach (var lane in lanes)
            {
                dists.Add(lane["distanceFromCenter"].Value);
            }
            return dists;
        }

        //TODO: Figure out target speeds given deceleration value for prior track pieces
        public void SetTargetSpeed(int pieceNum, double speed, double deceleration, SpeedCalculator calculator, int lane)
        {
            var turnPiece = _trackPieces.At(pieceNum) as TurnPiece;
            if(turnPiece != null)
            {
                var crashRadius = turnPiece.Radius(lane);
                Console.WriteLine("Crash speed/radius: " + speed + " " + crashRadius);
                Console.WriteLine("Speed attempted: " + turnPiece.TargetSpeed[lane]);
                for(int i = 0; i < _lanes; i++)
                {
                    var turnPieces = _trackPieces.Select((t, index) => new { Piece = t as TurnPiece, Index = index })
                        .Where(p => p.Piece != null);// && p.Piece.Radius(i) <= crashRadius);
                        //.Select(p => p.Index);
                    foreach(var t in turnPieces)
                    {
                        SetTargetPieceRecursive(t.Index, speed * (t.Piece.Radius(i) / crashRadius) * 0.83, deceleration, calculator, i);
                        //SetTargetPieceRecursive(t.Index, speed * (Math.Sqrt(t.Piece.Radius(i)) / Math.Sqrt(crashRadius)) * 0.84, deceleration, calculator, i);
                    }
                }
            }
            SetTargetPieceRecursive(pieceNum, speed * 0.83, deceleration, calculator, lane);
        }

        private void SetTargetPieceRecursive(int pieceNum, double speed, double deceleration, SpeedCalculator calculator, int lane)
        {
            if(pieceNum < _trackPieces.Count * -1)
                return;
            if (_trackPieces.At(pieceNum).TargetSpeed[lane] <= speed)
                return;
            _trackPieces.At(pieceNum).TargetSpeed[lane] = speed;
            double prevPieceSpeed = CalculatePrevPieceSpeed(pieceNum, speed, deceleration, calculator, lane);
            SetTargetPieceRecursive(pieceNum - 1, prevPieceSpeed, deceleration, calculator, lane);
        }

        private double CalculatePrevPieceSpeed(int pieceNum, double speed, double acceleration, SpeedCalculator calculator, int lane)
        {
         /*
            var s = _trackPieces.At(pieceNum - 1).Length(lane);
            var a = acceleration;
            var v = speed;
            return Math.Sqrt(v * v + (2 * a * s));
            */
            
            //if the length of the piece is greater than the length required to slow down
            //then we can set the required speed to max
            //Then when we reach the distance at which we need to start breaking the program switches to using the next piece speed.
            var prevPieceLength = _trackPieces.At(pieceNum - 1).Length(lane);

            if(prevPieceLength > calculator.DistanceToReachRequiredSpeed(calculator.EnginePower, speed)){
                return calculator.EnginePower;
            }
            //else determine Max speed of prev piece based on interating backwards
            return calculator.StartSpeedForDecelerationDistance(prevPieceLength, speed);
        }

        //private double CalculatePrevPieceSpeed(int

        //TODO: improve time efficiency if necessary
        public double DistanceToNextTurn(int pieceNum, double pieceDist, int lane)
        {
            var currentPieceNum = pieceNum;
            int nextTurnPiece;
            
            pieceNum++; //start from piece after current
            while (true)
            {
                if (_trackPieces.At(pieceNum) is TurnPiece)
                {
                    nextTurnPiece = pieceNum.SimplifyIndex(_trackPieces.Count);
                    break;
                }
                pieceNum++;
            }
            return DistanceBetween(currentPieceNum, pieceDist, nextTurnPiece, lane);
        }

        //TODO: improve time efficiency if necessary
        public double DistanceToNextSwitch(int pieceNum, double pieceDist, int lane)
        {
            var currentPieceNum = pieceNum;
            int nextSwitchPiece;
            
            pieceNum++; //start from piece after current
            while (true)
            {
                if (_trackPieces.At(pieceNum).IsSwitch)
                {
                    nextSwitchPiece = pieceNum.SimplifyIndex(_trackPieces.Count);
                    break;
                }
                pieceNum++;
            }
            return DistanceBetween(currentPieceNum, pieceDist, nextSwitchPiece, lane);
        }

        public double DistanceBetween(PiecePosition startPosition, PiecePosition endPosition)
        {
            if (startPosition.pieceIndex == endPosition.pieceIndex)
                return endPosition.inPieceDistance - startPosition.inPieceDistance;
            return DistanceBetween(startPosition.pieceIndex, startPosition.inPieceDistance, endPosition.pieceIndex, startPosition.lane.endLaneIndex) + endPosition.inPieceDistance;
        }

        public double DistanceBetween(int startPiece, double distThroughStartPiece, int endPiece, int lane)
        {
            int endPieceNum = endPiece.SimplifyIndex(_trackPieces.Count);
            double dist = _trackPieces.At(startPiece).Length(lane) - distThroughStartPiece;
            int pieceNum = ++startPiece;
            while (pieceNum.SimplifyIndex(_trackPieces.Count) != endPieceNum)
            {
                dist += _trackPieces.At(pieceNum).Length(lane);
                pieceNum++;
            }
            return dist;
        }

        private ITrackPiece CreateTrackPiece(dynamic piece, List<double> laneDistsFromCentre)
        {
            bool isSwitch = false;
            var switch_ = piece["switch"];
            if (switch_ != null)
                isSwitch = switch_.Value;
            var length = piece["length"];
            if (length != null)
                return new StraightPiece(length.Value, isSwitch, laneDistsFromCentre.Count);
            var radius = piece["radius"];
            var angle = piece["angle"];
            return new TurnPiece(radius.Value, angle.Value, isSwitch, laneDistsFromCentre);
        }

        private void calculateLaneSwitches()
        {
            double rightTurnDistanceBeforeSwitch = 0.0;
            double leftTurnDistanceBeforeSwitch = 0.0;
            bool firstSwitch = true;
            int prevSwitchIndex = 0;
            for (int i = 0; i < _trackPieces.Count; i++)
            {
                ITrackPiece currentPiece = _trackPieces.At(i);
                if (currentPiece is TurnPiece)
                {
                    double angle = (currentPiece as TurnPiece).Angle;
                    if (angle > 0.0)
                    {
                        rightTurnDistanceBeforeSwitch += currentPiece.Length(0);
                    }
                    else
                    {
                        leftTurnDistanceBeforeSwitch += currentPiece.Length(0);
                    }
                }

                if ((currentPiece.IsSwitch) || (i == _trackPieces.Count-1)) //switch piece or final piece
                {
                    if (firstSwitch)
                    {
                        prevSwitchIndex = i;
                        firstSwitch = false;
                        rightTurnDistanceBeforeSwitch = 0.0;
                        leftTurnDistanceBeforeSwitch = 0.0;
                    }
                    else
                    {
                        ITrackPiece prevSwitch = _trackPieces.At(prevSwitchIndex);
                        if (rightTurnDistanceBeforeSwitch > leftTurnDistanceBeforeSwitch)
                        {
                            prevSwitch.SwitchDirection = "Right";
                            Console.WriteLine("Switch at piece " + prevSwitchIndex + " Direction: Right");
                        }
                        else if (rightTurnDistanceBeforeSwitch < leftTurnDistanceBeforeSwitch)
                        {
                            prevSwitch.SwitchDirection = "Left";
                            Console.WriteLine("Switch at piece " + prevSwitchIndex + " Direction: Left");
                        }
                        else
                        {
							prevSwitch.SwitchDirection = "Straight";
                            Console.WriteLine("Switch at piece " + prevSwitchIndex + " Direction: Straight");
                        }

                        rightTurnDistanceBeforeSwitch = 0.0;
                        leftTurnDistanceBeforeSwitch = 0.0;
                        prevSwitchIndex = i;
                    }
                }
            }

        }

        internal double TargetSpeed(int pieceNum, int lane)
        {
            return _trackPieces.At(pieceNum).TargetSpeed[lane];
        }

		public string getSwitchDirection (int pieceNum)
		{
			if (_trackPieces.At (pieceNum).SwitchDirection != null)
			{
				return _trackPieces.At(pieceNum).SwitchDirection;
			}
			else
				return null;
		}

		public int getNextSwitchIndex (int pieceNum)
		{   
			int nextSwitchIndex;
            pieceNum++; //start from piece after current

            while (true)
            {
                if (_trackPieces.At(pieceNum).IsSwitch)
                {
					nextSwitchIndex = pieceNum.SimplifyIndex(_trackPieces.Count);
					break;
                }
                pieceNum++;
            }
            return nextSwitchIndex;
		}
    }

    public static class CircularListExtensions
    {
        public static T At<T>(this IList<T> circularList, int index)
        {
            if (index < 0)
            {
                var i = circularList.Count + index;
                if (i < 0)
                {
                    return circularList.At(i + circularList.Count);
                }
                return circularList[i];
            }
            return circularList[index % circularList.Count];
        }

        public static int SimplifyIndex(this int index, int count)
        {
            return index < 0 ? count + index : index % count;
        }
    }
}
