﻿using System;

namespace HWO.Messages
{
    public class TurboMsg : SendMsg
    {
        protected override string MsgType()
        {
            return "turbo";
        }

        protected override Object MsgData()
        {
            return "Nnnnnnnnnnnnnyyyyyyyeeowwwwwwwwwwwwwwww";
        }
    }
}
