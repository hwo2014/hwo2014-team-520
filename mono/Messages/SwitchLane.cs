using System;

namespace HWO.Messages
{
	public class SwitchLane : SendMsg
	{
		public string direction;

		public SwitchLane (string direction)
		{
			this.direction = direction;
		}

        protected override string MsgType()
        {
            return "switchLane";
        }

        protected override Object MsgData()
        {
			return direction;
        }
	}
}