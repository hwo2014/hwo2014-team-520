﻿using HWO.SerializableObjects;

namespace HWO.Messages
{
    public class JoinRace : SendMsg, IStartRace
    {
        public BotId botId;
        public string trackName;
        public string password;
        public int carCount;

        public JoinRace(BotId botId, string trackName, string password, int carCount)
        {
            this.botId = botId;
            this.trackName = trackName;
            this.password = password;
            this.carCount = carCount;
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }
}
