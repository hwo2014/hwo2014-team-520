﻿using HWO.SerializableObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWO
{
    public class SpeedCalculator
    {
        double _maxAcceleration;
        double _enginePower; //this might be called something else but MaxSpeed = Throttle*enginePower so enginePower seemed ok


        // Speed2 = Speed1 * (1- maxAcceleration/enginePower) + Throttle * maxAcceleration
        public double DetermineThrottle(double currentSpeed, double requiredSpeed)
        {

            //what do we do if they are the same?
            double throttle = (requiredSpeed - currentSpeed * (1 - this._maxAcceleration / this._enginePower)) / _maxAcceleration;
            if (throttle > 1.0)
            {
                throttle = 1.0;
            }
            //just to be safe
            if (throttle < 0.0)
            {
                throttle = 0.0;
            }
            return throttle;
        }


        //assumes straight path for acceleration/there are no speed constraints in the cars path
        public double DistanceToReachRequiredSpeed(double currentSpeed, double requiredSpeed)
        {
            //throttle = 1
            double distance = currentSpeed;
            if (requiredSpeed > currentSpeed)
            {
                double tempSpeed = currentSpeed;
                while (tempSpeed < requiredSpeed && tempSpeed < (this._enginePower - 0.01))
                {
                    double acceleration = this._maxAcceleration - (this._maxAcceleration / this._enginePower) * tempSpeed;
                    if (requiredSpeed - tempSpeed < acceleration)
                    {
                        tempSpeed = requiredSpeed;
                    }
                    else
                    {
                        tempSpeed = tempSpeed + acceleration;
                    }
                    distance += tempSpeed;
                }
            }
            else
            {

                double tempSpeed = currentSpeed;
                while (tempSpeed > requiredSpeed && tempSpeed > 0.1)
                {
                    double acceleration = -(this._maxAcceleration / this._enginePower) * tempSpeed;
                    if (tempSpeed + acceleration < 0.0)
                    {
                        tempSpeed = 0.0;
                    }
                    else
                    {
                        tempSpeed = tempSpeed + acceleration;
                    }
                    distance += tempSpeed;
                }
            }

            return distance;
        }


        internal double CalculateRequiredSpeed(double requiredSpeedForNextPiece, double requiredSpeedForThisPiece, double distanceToNextPiece)
        {
            if (requiredSpeedForNextPiece > requiredSpeedForThisPiece)
                return requiredSpeedForThisPiece;

            if (CanMaintainSpeedForAnotherTick(requiredSpeedForThisPiece, requiredSpeedForNextPiece, distanceToNextPiece))
                return requiredSpeedForThisPiece;
            else
                return requiredSpeedForNextPiece;
        }

        private bool CanMaintainSpeedForAnotherTick(double currentSpeed, double nextRequiredSpeed, double distanceToNextPiece)
        {
            return DistanceToReachRequiredSpeed(currentSpeed, nextRequiredSpeed) < distanceToNextPiece - currentSpeed;
        }

        public double MaxAcceleration { get { return _maxAcceleration; } set { _maxAcceleration = value; } }

        public double EnginePower { get { return _enginePower; } set { _enginePower = value; } }

        public bool EnginePowerCalculated { get { return _enginePower != 0; } }

        public double Speed1 { get; set; }

        public double Speed2 { get; set; }

        public double Speed0 { get; set; }

        internal void CalculateEnginePower()
        {
            var throttle = 1.0;
            var currentAccel = Speed2 - Speed1;
            var lastAccel = Speed1;//-speed0
            var changeInAccel = lastAccel - currentAccel;
            var changeInSpeed = Speed2 - Speed1;
            EnginePower = Math.Abs(((currentAccel / throttle) * (changeInSpeed / changeInAccel)) + (Speed2 / throttle));
            Console.WriteLine("Calculated engine power: " + EnginePower);
            //EnginePower = (lastAcel/throttle)*(change in speed)/(change in acel)+(currentSpeed/throttle)
        }

        public double StartSpeedForDecelerationDistance(double distance, double endSpeed)
        {
            double distanceBackward = 0;
            double startSpeed = endSpeed;

            while (distanceBackward < distance)
            {
                double acceleration = (this._maxAcceleration / this._enginePower) * startSpeed;
                distanceBackward = distanceBackward + startSpeed + acceleration;
                if (distanceBackward < distance)
                {
                    startSpeed = startSpeed + acceleration;
                }
            }
            return startSpeed;
        }
    }
}

