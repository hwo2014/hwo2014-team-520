﻿using System.Collections.Generic;
namespace HWO
{
    public interface ITrackPiece
    {
        bool IsSwitch { get; }

        /// <summary>
        /// Max speed at which it is safe to take the piece to not fall off here or later.
        /// Speed may not be the best measure, but it may do for now!
        /// </summary>
        List<double> TargetSpeed { get; set; }

        string SwitchDirection { get; set; }

        double Length(int lane);
    }
}
