﻿using System;
using System.Collections.Generic;

namespace HWO
{
    public class TurnPiece : ITrackPiece
    {
        private double _radius;
        private bool _isSwitch;
        private double _length;
        private double _angle;
        private List<double> _targetSpeed = new List<double>();
        private List<double> _laneDistsFromCentre;

        public TurnPiece(double radius, double angle, bool isSwitch, List<double> laneDistsFromCentre)
        {
            _radius = radius;
            _isSwitch = isSwitch;
            _angle = angle;
            _length = 2 * Math.PI * radius * (Math.Abs(angle) / 360);
            _laneDistsFromCentre = laneDistsFromCentre;
            for(int i = 0; i < laneDistsFromCentre.Count; i++)
            {
                _targetSpeed.Add(double.MaxValue);
            }
        }

        public bool IsSwitch
        {
            get { return _isSwitch; }
        }

        public double Length(int lane)
        {
            return 2 * Math.PI * (_radius + _laneDistsFromCentre[lane]) * (Math.Abs(_angle) / 360);
        }

        public double Angle
        {
            get { return _angle;}
        }

        public double Radius(int lane)
        {
            return _radius + _laneDistsFromCentre[lane];
        }


        public List<double> TargetSpeed
        {
            get
            {
                return _targetSpeed;
            }
            set
            {
                _targetSpeed = value;
            }
        }

        public string SwitchDirection { get; set; }
    }
}
