﻿
using System.Collections.Generic;
namespace HWO
{
    class StraightPiece : ITrackPiece
    {
        private double _length;
        private bool _isSwitch;
        private List<double> _targetSpeed = new List<double>();
        private string _switchDirection;

        public StraightPiece(double length, bool isSwitch, int lanes)
        {
            _length = length;
            _isSwitch = isSwitch;
            for(int i = 0; i < lanes; i++)
            {
                _targetSpeed.Add(double.MaxValue);
            }
        }

        public bool IsSwitch
        {
            get { return _isSwitch; }
        }


        public double Length(int lane)
        {
             return _length;
        }

        public List<double> TargetSpeed
        {
            get
            {
                return _targetSpeed;
            }
            set
            {
                _targetSpeed = value;
            }
        }

        public string SwitchDirection
        {
            get
            {
                return _switchDirection;
            }
            set
            {
                _switchDirection = value;
            }
        }
    }
}
