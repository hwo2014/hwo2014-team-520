﻿using System;

namespace HWO.SerializableObjects
{
    [Serializable]
    public class BotId
    {
        public string name;
        public string key;

        public BotId(string name, string key)
        {
            this.name = name;
            this.key = key;
        }
    }
}
