﻿
namespace HWO.SerializableObjects
{
    public class Turbo
    {
        private bool _isAvailable;

        public double turboDurationMilliseconds;
        public int turboDurationTicks;
        public double turboFactor;

        public Turbo(double turboDurationMilliseconds, int turboDurationTicks, double turboFactor)
        {
            this.turboDurationMilliseconds = turboDurationMilliseconds;
            this.turboDurationTicks = turboDurationTicks;
            this.turboFactor = turboFactor;
            _isAvailable = true;
        }

        public bool IsAvailable { get { return _isAvailable; } }

        public void Use()
        {
            _isAvailable = false;
        }
    }
}
