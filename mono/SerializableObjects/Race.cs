﻿
namespace HWO.SerializableObjects
{
    public class Race
    {
        public Track track;
        public Car[] cars;
        public RaceSession raceSession;
    }
}
