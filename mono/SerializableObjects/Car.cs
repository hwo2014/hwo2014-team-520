﻿using System;
using System.Runtime.CompilerServices;

namespace HWO.SerializableObjects
{
    public class Car
    {
        private CarPosition _carPosition;
        private double _speed;

        public string color;
        public double length;

        public Car(dynamic id, dynamic dimensions)
        {
            color = id["color"].ToString();
            length = dimensions["width"].Value;
        }

        public double Speed { get { return _speed; } }

        internal void SetPosition(CarPosition position, TrackData trackData)
        {
            var prevCarPosition = _carPosition;
            _carPosition = position;
            if (prevCarPosition == null || _carPosition == null)
                _speed = 0;
            else
                _speed = trackData.DistanceBetween(prevCarPosition.piecePosition, _carPosition.piecePosition);
            if (_speed < 0)
                Console.WriteLine("Speed: " + _speed);
        }

        public CarPosition Position { get { return _carPosition; } }
    }
}
