﻿
namespace HWO.SerializableObjects
{
    public class PiecePosition
    {
        public int pieceIndex;
        public double inPieceDistance;
        public Lane lane;
        public int lap;
    }

    public class Lane
    {
        public int startLaneIndex;
        public int endLaneIndex;
    }
}
