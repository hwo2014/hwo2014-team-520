﻿
namespace HWO.SerializableObjects
{
    public class CarPosition
    {
        public dynamic id;
        public double angle;
        public PiecePosition piecePosition;
    }
}
