﻿using HWO.SerializableObjects;
using System.Collections.Generic;
using Color = System.String;
using System.Linq;

namespace HWO
{
    public class CarData
    {
        private Dictionary<Color, Car> _cars = new Dictionary<Color, Car>();

        public CarData(Car[] cars)
        {
            foreach (var car in cars)
            {
                _cars.Add(car.color, car);
            }
        }

        public void SetCarPositions(CarPosition[] positions, TrackData trackData)
        {
            foreach(var position in positions)
            {
                ((Car)(_cars[position.id["color"].Value])).SetPosition(position, trackData);
            }
        }

        public double Speed(string color)
        {
            return _cars[color].Speed;
        }

        public double CarLength { get { return _cars.First().Value.length; } }

        public CarPosition Position(string color)
        {
            return _cars[color].Position;
        }

		public Dictionary<Color, Car> getCars()
		{
			return _cars;
		}

        public bool IsCarCloseBehind(Color myCar, TrackData trackData)
        {
            var myPosition = Position(myCar);
            var otherCarDistances = _cars.Keys.Except(_cars.Keys.Where(c=> c == myCar)).
                Select(colour => trackData.DistanceBetween(Position(colour).piecePosition, Position(myCar).piecePosition));
            if (!otherCarDistances.Any())
                return false;
            var closestCar = otherCarDistances.Min();
            return (closestCar < CarLength * 2);
        }

        public int Lane(Color color)
        {
            var myPosition = Position(color).piecePosition;
            return myPosition.lane.endLaneIndex;
        }
    }
}
