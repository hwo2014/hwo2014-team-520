using HWO.Messages;
using HWO.SerializableObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;

namespace HWO
{
    public class Launcher
    {
        public static void Main(string[] args)
        {
            string host;
            int port;
            string botName = "Blobfish";
            string botKey = "kF6rZeVmCTOCNw";
            Console.WriteLine(string.Join(" ", args));

            host = args[0];
            port = int.Parse(args[1]);
            if(args.Length > 2)
                botName = args[2];
            if(args.Length > 3)
                botKey = args[3];
            if(args.Length > 4 && args[4] == "human")
                HumanLauncher(host, port, botName, botKey);
            else
                QuickRace(host, port, botName, botKey);
        }

        private static void HumanLauncher(string host, int port, string botName, string botKey)
        {
            Console.WriteLine("------------Blobfish bot launcher------------");
            Console.WriteLine();
            Console.WriteLine(string.Format("1. Quick race on {0}", host));
            Console.WriteLine(string.Format("2. Create race"));
            Console.WriteLine(string.Format("3. Join race"));
            Console.WriteLine();
            int selection;
            if (int.TryParse(Console.ReadLine(), out selection))
            {
                switch(selection)
                {
                    case 1:
                        QuickRace(host, port, botName, botKey);
                        break;
                    case 2:
                        host = GetHost();
                        if (host == null)
                            break;
                        StartRace(host, port, botKey, true);
                        break;
                    case 3:
                        host = GetHost();
                        if (host == null)
                            break;
                        StartRace(host, port, botKey, false);
                        break;
                    default:
                        break;
                }
            }
            Console.WriteLine("Enter any key to close window.");
            Console.Read();
        }

        private static string GetHost()
        {
            Console.WriteLine("---Choose a server---");
            int optionNum = 1;
            var hostsFile = Path.Combine(Directory.GetCurrentDirectory(), "hosts.txt");
            List<string> hosts = new List<string>();
            if(File.Exists(hostsFile))
            {
                var h = File.ReadAllLines(hostsFile);
                foreach(var host in h)
                {
                    hosts.Add(host);
                    Console.WriteLine(string.Format("{0}: {1}", optionNum++, host));
                }
            }
            Console.WriteLine(string.Format("{0}: Add new host server", optionNum));
            int selection;
            if(!(int.TryParse(Console.ReadLine(), out selection)))
                return null;
            if (selection == optionNum)
            {
                Console.WriteLine("Enter new host:");
                var newHost = Console.ReadLine();
                hosts.Add(newHost);
                File.WriteAllLines(hostsFile, hosts);
                return newHost;
            }
            else if (selection > 0 && selection <= hosts.Count)
            {
                return hosts[selection - 1];
            }
            else
                return null;

        }

        private static void QuickRace(string host, int port, string botName, string botKey)
        {
            Console.WriteLine(string.Format("Connecting to {0}:{1} as {2}/{3}", host, port, botName, botKey));
            LaunchBot(host, port, new Join(botName, botKey));
        }

        private static void StartRace(string host, int port, string botKey, bool isNewRace)
        {
            Console.WriteLine("Bot name: ");
            var botName = Console.ReadLine();
            Console.WriteLine("[Track name] [Car count] [Password (optional)]");
            var input = Console.ReadLine().Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            if(input.Length < 2)
                return;
            var trackName = input[0].ToLower();
            int carCount;
            if(!int.TryParse(input[1], out carCount))
                return;
            string password = null;
            if(input.Length >= 3)
                password = input[2];

            IStartRace startDetails;
            var botId = new BotId(botName, botKey);
            if (isNewRace)
            {
                Console.WriteLine(string.Format("Starting new race on {0}:{1} as {2}/{3}", host, port, botName, botKey));
                startDetails = new CreateRace(botId, trackName, password, carCount);
            }
            else
            {
                Console.WriteLine(string.Format("Joining race on {0}:{1} as {2}/{3}", host, port, botName, botKey));
                startDetails = new JoinRace(botId, trackName, password, carCount);
            }
            LaunchBot(host, port, startDetails);
        }

        private static void LaunchBot(string host, int port, IStartRace startDetails)
        {
            using (TcpClient client = new TcpClient(host, port))
            {
                NetworkStream stream = client.GetStream();
                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                new bot(reader, writer, startDetails);
            }
        }
    }
}